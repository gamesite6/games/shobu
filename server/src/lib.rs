use rand::prelude::*;
use serde::{Deserialize, Serialize};

use enum_iterator::IntoEnumIterator;
use std::collections::HashSet;

pub type PlayerId = u32;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct GameSettings {}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Eq)]
#[serde(rename_all = "camelCase")]
pub struct GameState {
    players: Players,
    boards: [Board; 4],
    phase: Phase,
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Eq)]
#[serde(rename_all = "camelCase")]
pub struct Players(PlayerId, PlayerId);
impl Players {
    fn white(&self) -> PlayerId {
        self.0
    }
    fn black(&self) -> PlayerId {
        self.1
    }
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Eq)]
#[serde(tag = "t", rename_all = "camelCase")]
pub enum Phase {
    Passive { player: PlayerColor },
    Aggressive { passive: Move, player: PlayerColor },
    GameComplete { winner: PlayerId },
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize, PartialEq, Eq)]
#[serde(rename_all = "camelCase")]
pub struct Move {
    board: usize,
    from: Square,
    to: Square,
}

#[derive(Clone, Copy, Debug, Eq, PartialEq, Hash)]
pub enum BoardColor {
    Dark,
    Light,
}

#[derive(Clone, Debug, Serialize, Deserialize, Eq, PartialEq)]
#[serde(tag = "t", rename_all = "camelCase")]
pub enum Action {
    Passive { mv: Move },
    UndoPassive,
    Aggressive { board: usize, square: Square },
}

impl Action {
    fn is_allowed(&self, user_id: PlayerId, state: &GameState) -> bool {
        match (self, &state.phase) {
            (
                Action::Passive {
                    mv:
                        Move {
                            board: board_idx,
                            from,
                            to,
                        },
                },
                Phase::Passive {
                    player: player_color,
                },
            ) => {
                let acting_player_id: PlayerId = match player_color {
                    PlayerColor::Black => state.players.black(),
                    PlayerColor::White => state.players.white(),
                };

                if acting_player_id != user_id
                    || Board::home_color(*board_idx) != Some(*player_color)
                {
                    return false;
                }

                if let Some(board) = state.boards.get(*board_idx) {
                    if let Some(stone) = board.get_stone(from) {
                        return stone.color() == *player_color
                            && Board::home_color(*board_idx) == Some(*player_color)
                            && from.in_bounds()
                            && to.in_bounds()
                            && from.valid_path(to)
                            && from.path(to).count() < 3
                            && from
                                .path(to)
                                .all(|square| board.get_stone(&square).is_none());
                    }
                }

                false
            }
            (Action::Passive { .. }, _) => false,

            (Action::UndoPassive, Phase::Aggressive { player, .. }) => {
                let acting_player_id = match player {
                    PlayerColor::Black => state.players.black(),
                    PlayerColor::White => state.players.white(),
                };
                acting_player_id == user_id
            }
            (Action::UndoPassive, _) => false,

            (
                Action::Aggressive {
                    board: board_idx,
                    square,
                },
                Phase::Aggressive { passive, player },
            ) => {
                let acting_player = match player {
                    PlayerColor::Black => state.players.black(),
                    PlayerColor::White => state.players.white(),
                };
                if let Some(board) = state.boards.get(*board_idx) {
                    if let Some(stone) = board.get_stone(square) {
                        let square_diff = passive.to - passive.from;
                        let goal = *square + square_diff;

                        let pushed_stone = square.path(&goal).find_map(|sq| board.get_stone(&sq));

                        return acting_player == user_id
                            && stone.color() == *player
                            && Board::board_color(*board_idx) != Board::board_color(passive.board)
                            && square.in_bounds()
                            && goal.in_bounds()
                            && square
                                .path_plus_one(&goal)
                                .filter(|sq| sq.in_bounds())
                                .filter_map(|sq| board.get_stone(&sq))
                                .count()
                                < 2
                            && if let Some(stone) = pushed_stone {
                                stone.color() != *player
                            } else {
                                true
                            };
                    }
                }

                false
            }
            (Action::Aggressive { .. }, _) => false,
        }
    }
}

#[cfg(test)]
mod action_test {
    use super::*;

    #[test]
    fn serde() {
        let initial_action = Action::Passive {
            mv: Move {
                board: 0,
                from: Square(0, 2),
                to: Square(0, 3),
            },
        };

        let value = serde_json::to_value(initial_action.clone()).unwrap();
        let action = serde_json::from_value(value).unwrap();
        assert_eq!(initial_action, action)
    }
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct Board(Vec<(Square, Stone)>);

impl Board {
    fn new() -> Self {
        Self(vec![
            (Square(0, 0), Stone(0)),
            (Square(1, 0), Stone(1)),
            (Square(2, 0), Stone(2)),
            (Square(3, 0), Stone(3)),
            (Square(0, 3), Stone(4)),
            (Square(1, 3), Stone(5)),
            (Square(2, 3), Stone(6)),
            (Square(3, 3), Stone(7)),
        ])
    }
    fn get_stone(&self, square: &Square) -> Option<&Stone> {
        self.0
            .iter()
            .find(|(sq, _)| sq == square)
            .map(|(_, stone)| stone)
    }
    fn take_stone(&mut self, square: &Square) -> Option<Stone> {
        let idx = self.0.iter().position(|(sq, _)| sq == square)?;
        let (_, stone) = self.0.remove(idx);
        Some(stone)
    }

    fn place_stone(&mut self, square: &Square, stone: Stone) -> Result<(), ()> {
        match self.0.iter().find(|(sq, _)| sq == square) {
            Some(_) => Err(()),
            None => {
                self.0.push((*square, stone));
                Ok(())
            }
        }
    }

    fn home_color(board: usize) -> Option<PlayerColor> {
        match board {
            0 | 1 => Some(PlayerColor::White),
            2 | 3 => Some(PlayerColor::Black),
            _ => None,
        }
    }

    fn board_color(board: usize) -> Option<BoardColor> {
        match board {
            0 | 2 => Some(BoardColor::Dark),
            1 | 3 => Some(BoardColor::Light),
            _ => None,
        }
    }
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct Stone(u8);
impl Stone {
    fn color(&self) -> PlayerColor {
        if self.0 < 4 {
            PlayerColor::White
        } else {
            PlayerColor::Black
        }
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub struct Square(i8, i8);
impl Square {
    fn x(&self) -> i8 {
        self.0
    }
    fn y(&self) -> i8 {
        self.1
    }
}

#[cfg(test)]
mod square_serde_test {
    use super::*;
    use serde_json::*;

    #[test]
    fn serialize() {
        assert_eq!(serde_json::to_value(Square(1, 2)).unwrap(), json!([1, 2]))
    }

    #[test]
    fn deserialize() {
        assert_eq!(
            serde_json::from_value::<Square>(json!([3, -1])).unwrap(),
            Square(3, -1)
        )
    }
}

impl Square {
    fn in_bounds(&self) -> bool {
        matches!(self, Square(0..=3, 0..=3))
    }

    fn valid_path(&self, to: &Square) -> bool {
        self.path(to).next().is_some()
    }

    fn path(&self, goal: &Square) -> SquareIterator {
        SquareIterator {
            previous: *self,
            goal: *goal,
        }
    }
    fn path_plus_one(&self, goal: &Square) -> SquareIterator {
        if self.valid_path(goal) {
            let diff = *self - *goal;
            let goal = Square(
                goal.x() - num::signum(diff.x()),
                goal.y() - num::signum(diff.y()),
            );
            self.path(&goal)
        } else {
            self.path(goal)
        }
    }
}
impl std::ops::Sub for Square {
    type Output = Square;
    fn sub(self, rhs: Self) -> Self::Output {
        Square(self.x() - rhs.x(), self.y() - rhs.y())
    }
}
impl std::ops::Add for Square {
    type Output = Square;
    fn add(self, rhs: Self) -> Self::Output {
        Square(self.x() + rhs.x(), self.y() + rhs.y())
    }
}
pub struct SquareIterator {
    previous: Square,
    goal: Square,
}
impl Iterator for SquareIterator {
    type Item = Square;

    fn next(&mut self) -> Option<Self::Item> {
        match self.goal - self.previous {
            Square(0, 0) => None,
            Square(x_diff, y_diff)
                if x_diff == 0 || y_diff == 0 || x_diff.abs() == y_diff.abs() =>
            {
                let square = Square(
                    self.previous.x() + num::signum(x_diff),
                    self.previous.y() + num::signum(y_diff),
                );
                self.previous = square;
                Some(square)
            }
            _ => None,
        }
    }
}

#[cfg(test)]
mod square_tests {
    use super::*;

    #[test]
    fn horizontal_path() {
        assert_eq!(
            Square(2, 0).path(&Square(2, 3)).collect::<Vec<_>>(),
            vec![Square(2, 1), Square(2, 2), Square(2, 3),]
        );
    }
    #[test]
    fn vertical_path() {
        assert_eq!(
            Square(1, 1).path(&Square(3, 1)).collect::<Vec<_>>(),
            vec![Square(2, 1), Square(3, 1),]
        );
    }

    #[test]
    fn diagonal_path() {
        assert_eq!(
            Square(0, 3).path(&Square(3, 0)).collect::<Vec<_>>(),
            vec![Square(1, 2), Square(2, 1), Square(3, 0),]
        )
    }

    #[test]
    fn invalid_path() {
        assert_eq!(Square(0, 3).path(&Square(1, 0)).collect::<Vec<_>>(), vec![])
    }

    #[test]
    fn path_plus_one() {
        assert_eq!(
            Square(0, 3)
                .path_plus_one(&Square(3, 0))
                .collect::<Vec<_>>(),
            vec![Square(1, 2), Square(2, 1), Square(3, 0), Square(4, -1),]
        )
    }
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize, Eq, PartialEq, Hash, IntoEnumIterator)]
pub enum PlayerColor {
    #[serde(rename = "b")]
    Black,

    #[serde(rename = "w")]
    White,
}

impl PlayerColor {
    fn invert(&self) -> Self {
        match self {
            PlayerColor::Black => PlayerColor::White,
            PlayerColor::White => PlayerColor::Black,
        }
    }
}

impl GameState {
    fn new(players: Players) -> GameState {
        let boards = [Board::new(), Board::new(), Board::new(), Board::new()];
        let phase = Phase::Passive {
            player: PlayerColor::Black,
        };

        Self {
            players,
            boards,
            phase,
        }
    }
    pub fn initial_state(player_ids: &[PlayerId], seed: i64) -> Option<Self> {
        let mut player_ids = player_ids.iter().copied().collect::<Vec<_>>();
        if player_ids.len() != 2 {
            return None;
        }

        let mut rng = rand_pcg::Pcg32::seed_from_u64(seed.unsigned_abs());
        player_ids.shuffle(&mut rng);

        assert_ne!(player_ids[0], player_ids[1]);

        let players = Players(player_ids[0], player_ids[1]);
        Some(GameState::new(players))
    }
    pub fn is_game_complete(&self) -> bool {
        matches!(self.phase, Phase::GameComplete { .. })
    }
    pub fn perform_action(&self, action: &Action, user_id: PlayerId) -> Option<Self> {
        let actor_color: PlayerColor = if self.players.white() == user_id {
            PlayerColor::White
        } else if self.players.black() == user_id {
            PlayerColor::Black
        } else {
            return None;
        };

        if !action.is_allowed(user_id, self) {
            return None;
        }

        let mut next_state = self.clone();

        match (&self.phase, action) {
            (Phase::Passive { .. }, &Action::Passive { mv: action_move }) => {
                let Move { board, to, from } = action_move;
                let board_mut = next_state.boards.get_mut(board)?;
                let stone = board_mut.take_stone(&from)?;

                if board_mut.place_stone(&to, stone).is_err() {
                    return None;
                }

                next_state.phase = Phase::Aggressive {
                    passive: action_move,
                    player: actor_color,
                }
            }
            (Phase::Passive { .. }, _) => return None,

            (Phase::Aggressive { player, passive }, Action::UndoPassive) => {
                let board_mut = next_state.boards.get_mut(passive.board)?;

                let stone = board_mut.take_stone(&passive.to)?;
                if board_mut.place_stone(&passive.from, stone).is_err() {
                    return None;
                }

                next_state.phase = Phase::Passive { player: *player }
            }
            (
                Phase::Aggressive { player, passive },
                Action::Aggressive {
                    board: board_idx,
                    square,
                },
            ) => {
                let goal = *square + (passive.to - passive.from);
                let goal_plus_one = square.path_plus_one(&goal).last()?;

                let board = next_state.boards.get_mut(*board_idx)?;
                board.0.retain(|(sq, _stone)| sq.in_bounds());

                let moved_stone = board.take_stone(square)?;
                let pushed_stone = square.path(&goal).find_map(|sq| board.take_stone(&sq));

                board.place_stone(&goal, moved_stone).ok()?;
                if let Some(pushed_stone) = pushed_stone {
                    board.place_stone(&goal_plus_one, pushed_stone).ok()?;
                }

                if let Some(winner) = next_state.get_winner() {
                    let winner = match winner {
                        PlayerColor::Black => next_state.players.black(),
                        PlayerColor::White => next_state.players.white(),
                    };

                    next_state.phase = Phase::GameComplete { winner };
                } else {
                    next_state.phase = Phase::Passive {
                        player: player.invert(),
                    }
                }
            }
            (Phase::Aggressive { .. }, _) => return None,

            (Phase::GameComplete { .. }, _) => return None,
        }

        Some(next_state)
    }

    fn get_winner(&self) -> Option<PlayerColor> {
        for board in self.boards.iter() {
            let colors = board
                .0
                .iter()
                .filter(|&(square, _stone)| square.in_bounds())
                .map(|(_square, stone)| stone.color())
                .collect::<HashSet<PlayerColor>>();

            if !colors.contains(&PlayerColor::Black) {
                return Some(PlayerColor::White);
            } else if !colors.contains(&PlayerColor::White) {
                return Some(PlayerColor::Black);
            }
        }

        None
    }
}
#[cfg(test)]
mod state_tests {
    use super::*;

    #[test]
    fn state_to_json() {
        let initial_state = GameState::new(Players(1, 2));
        let json = serde_json::to_value(&initial_state).unwrap();
        let state = serde_json::from_value(json).unwrap();

        assert_eq!(initial_state, state);
    }
}
