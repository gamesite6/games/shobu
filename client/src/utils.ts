export function isActing(
  phase: Phase,
  playerColor: PlayerColor | null
): boolean {
  switch (phase.t) {
    case "passive":
      return playerColor === phase.player;
    case "aggressive":
      return playerColor === phase.player;
    case "gameComplete":
      return false;
  }
}

export function boardPlayerSide(boardIdx: number): PlayerColor {
  return boardIdx < 2 ? "w" : "b";
}

export function whitePlayerId(state: GameState): PlayerId {
  return state.players[0];
}
export function blackPlayerId(state: GameState): PlayerId {
  return state.players[1];
}

export function stoneColor(stone: Stone): PlayerColor {
  if (stone < 4) {
    return "w";
  } else {
    return "b";
  }
}

export function squareEquals([x1, y1]: Square, [x2, y2]: Square): boolean {
  return x1 === x2 && y1 === y2;
}

export function getDestinationSquares(
  stones: Board,
  activeSquare: Square
): Square[] {
  let [activeX, activeY] = activeSquare;

  let candidateSquares = [-2, -1, 0, 1, 2].flatMap((deltaX) =>
    [-2, -1, 0, 1, 2].map(
      (deltaY) => [activeX + deltaX, activeY + deltaY] as Square
    )
  );

  function isEmptySquare(s: Square): boolean {
    let [x, y] = s;
    const validCoords = x >= 0 && x <= 3 && y >= 0 && y <= 3;

    return (
      validCoords &&
      !stones.some(([boardSquare, _stone]) => squareEquals(boardSquare, s))
    );
  }

  return candidateSquares.filter((c: Square) => {
    const path = getPath(activeSquare, c);
    return path !== null && path.every((p) => isEmptySquare(p));
  });
}

export function getPath(from: Square, to: Square): Square[] | null {
  let [fromX, fromY] = from;
  let [toX, toY] = to;
  let stepX = Math.sign(toX - fromX);
  let stepY = Math.sign(toY - fromY);

  let firstStep = [fromX + stepX, fromY + stepY] as Square;
  if (squareEquals(firstStep, to)) {
    return [firstStep];
  }

  let secondStep = [firstStep[0] + stepX, firstStep[1] + stepY] as Square;
  if (squareEquals(secondStep, to)) {
    return [firstStep, secondStep];
  } else {
    return null;
  }
}
