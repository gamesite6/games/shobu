import { defineConfig } from "vite";
import { svelte } from "@sveltejs/vite-plugin-svelte";

// https://vitejs.dev/config/
export default defineConfig({
  base: "/static/push-out/",
  build: {
    emptyOutDir: false,
    lib: {
      entry: "src/main.ts",
      fileName: "push-out",
      name: "Gamesite6_PushOut",
    },
  },
  css: {
    modules: {},
  },
  plugins: [svelte()],
});
