type PlayerId = number;

type GameSettings = {};

type GameState = { players: Players; boards: Board[]; phase: Phase };

type Board = [Square, Stone][];

type Square = [number, number];

type Stone = number;

type Players = [PlayerId, PlayerId];

type PlayerColor = "b" | "w";
type Phase =
  | { t: "passive"; player: PlayerColor }
  | { t: "aggressive"; passive: Move; player: PlayerColor }
  | { t: "gameComplete"; winner: PlayerId };

type Action =
  | { t: "passive"; mv: Move }
  | { t: "undoPassive" }
  | { t: "aggressive"; board: number; square: Square };

type Move = { board: number; from: Square; to: Square };
